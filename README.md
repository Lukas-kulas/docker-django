# docker-django

#1 Build
docker-compose build

#2 Set up
docker-compose up

#3 Restart
docker-compose stop && docker-compose start

#4 Shell
docker exec -ti "container_name" bash

#5 Collect static
docker exec web-container /bin/sh -c "python manage.py collectstatic --noinput" 